# VisualiseDictionary #

This module aims to provide a simple, quick and easy way of visualising a dictionary of dictionaries (of dictionaries...). There is a simple "pretty print" of the keys to the terminal, as well as a more advanced plot of the keys using pygraphviz.

Due to this being an extension of pygraphviz, it is possible to completely customise the plots using the [methods described on their site](http://pygraphviz.github.io).  

## Examples ##
There are examples for how to use this package in the ./ipynb directory. These are as an iPython notebook. The HTML output is available [here](https://bitbucket.org/alex-warwickvesztrocy/python-visualisedictionary/downloads/Examples.html).
